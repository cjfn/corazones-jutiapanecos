<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inventario</title>
<link href="../css/estilo.css" rel="stylesheet">
<script src="../js/jquery.js"></script>
<script src="../js/myjava_adopcion.js"></script>
<link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../bootstrap/css/bootstrap-theme.css" rel="stylesheet">
<link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
</head>
<body>
<br />
<br />



<div class="navbar  navbar-default navbar-fixed-top">

	
    	<div class = "container">
        <a href="#" class="navbar-brand" CLASS="alert-warning"> Menu </a>
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">

        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
   
                        
        </button>
        <div class ="collapse navbar-collapse navHeaderCollapse">
        	
            <ul class = "nav navbar-nav navbar-right">
            <li class = "active"> <a href="../index.php">HOME</a></li>
           
            	
           
                
             
           <li class=" bg-success"> <a href="vistas/adopcion.php"> HACER ADOPCION </a></li> 
                      
                </ul>                                   
            </ul>
            
        </div>
    </div>
</div>


    <header>Formulario de solicitud de adopcion</header>
    <div class="container">
<section class="main row">

    <article class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
    <h1>Al llenar firmar este formulario, el adoptante se compromete a:</h1>
    <h5><li>Continuar con los cuidados médicos y tratamientos que están en curso (si fuera el
caso) se compromete a llevar un control sanitario del animal, proporcionándole
sus vacunas pertinentes, desparasitaciones, revisiones, y asistencia veterinaria en
caso de contraer cualquier enfermedad o accidente.</li></h5>
    
    <h5><li>Proveerle todo los días agua limpia. Concentrado de calidad y dárselo de acuerdo
a su peso, en raciones distribuidas en dos tiempos.</li></h5>
    
    <h5><li>Proveerle de platos para su alimentación (agua y concentrado) - No darle alimentos
de humanos (Son veneno y peligroso para los perros: las uvas, el chocolate, la
cebolla, la manzana, las pasas, huesos de pollo).</li></h5>
    
    <h5><li>La Mascota no será en ningún caso golpeado, maltratado, amarrado, abandonado
ni regalado.</li></h5>
    
    <h5><li>Si el adoptado se entregara sin esterilizar, es obligatorio y por cuenta del adoptante
la esterilización del mismo, en cuanto cumpla los seis meses si es un cachorro o bien
en el plazo máximo de dos meses tras la adopción si su edad es superior a 6 meses.
Bajo ningún concepto, hará criar al animal, ni le utilizará para ningún tipo de fines
económicos.</li></h5>
    
    <h5><li>La Asociación que me entrega la mascota en adopción pueden efectuar llamadas
y visitas a mi residencia para constatar el bienestar de la mascota y solicitar fotografías
de la mascota adoptada.</li></h5>
    
    <h5><li>Siempre que la Asociación decida recoger de nuevo al adoptado, éste será
entregado de buen grado por parte del Adoptante, sin dañar su integridad física,
ni psíquica.</li></h5>
    
    <h5><li>La Asociación se reserva el derecho de retirar la custodia del adoptado al Adoptante
si lo considera oportuno por estimar que éste no está adecuadamente atendido.</li></h5>
    
    <h5><li>Contactar a la Asociación que me dio en adopción al perro/a o gato/a y la
devolveré si por alguna razón ésta no se adapta en mi hogar o en caso de ya no
pueda tenerla o si el perro adoptado no está en un medio apropiado y no puedo
atenderlo.</li></h5>
    
 
    
    
    
</article>
    
    
    
    

  
    
    
    <article class="col-xs-12 col-sm-8 col-md-4 col-lg-3">

    <center>
          
    <img src="../IMGS/logo corazones.png" />
    </center>
    </article>
    </section>
          </div>
    <section>
    
    </div>
    <center>
      <td width="100"><button id="nueva-adopcion" class="btn btn-primary">LLENAR FORMULARIO</button></td>
            
    
    </center>
    
    <!-- MODAL PARA EL REGISTRO DE PRODUCTOS-->
    <div class="modal fade" id="registra-adopcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel"><b>Adopcion</b></h4>
            </div>
            <form id="formulario" class="formulario" onsubmit="return modificaAdopcion();">
            <div class="modal-body">
				<table border="0" width="100%">
               		 <tr>
                        <td colspan="2"><input type="text" required="required" readonly="readonly" id="id_adop" name="id_adop" readonly="readonly" style="visibility:hidden; height:5px;"/></td>
                    </tr>
                     <tr>
                    	<td width="150">Proceso: </td>
                        <td><input type="text" required="required" readonly="readonly" id="pro" name="pro"/></td>
                    </tr>
                	<tr>
                    	<td>Seleccione o ingrese el codigo de la mascota a adoptar</td>
                        <td>
                            <SELECT name="mascota" id="mascota">
<option>Seleccione una Mascota...</option>
<?php 
$conexion=mysql_connect("localhost","root","") or
die("Problemas en la conexion");
mysql_select_db("corazones",$conexion) or
die("Problemas en la selección de la base de datos");  
mysql_query ("SET NAMES 'utf8'");
$clavebuscadah=mysql_query("select id_mascota, nombre, tipo_mascota from mascotas",$conexion) or
die("Problemas en el select:".mysql_error());
while($row = mysql_fetch_array($clavebuscadah))
{
echo'<OPTION VALUE="'.$row['id_mascota'].'">'.$row['nombre'].'   -   -   -   -   -   -   - '.$row['tipo_mascota'].'</OPTION>';
}
 
?>
</SELECT></td>
                    </tr>
            
                                    <tr>
                    	<td>Nombre del adoptante </td>
                        <td>
                            <input type="text" required="required" name="nombre" id="nombre" maxlength="100"/>
                            
                            
                            
                            </td>
                    </tr>
                    <tr>
                    	<td>Correo </td>
                        <td><input type="text" required="required" name="correo" id="correo"/></td>
                    </tr>
                    <tr>
                    	<td>Edad:</td>
                        <td><input type="number"  required="required" name="edad" id="edad"/></td>
                    </tr>
                                    
                     <tr>
                    	<td>Telefono</td>
                        <td><input type="text"  required="required" name="telefono" id="telefono"/></td>
                    </tr>
                                    
                                                     
                     <tr>
                    	<td>Celular</td>
                        <td><input type="text"  required="required" name="celular" id="celular"/></td>
                    </tr>  
                                    
                                     
                     <tr>
                    	<td>Dirección</td>
                        <td><input type="text"  required="required" name="direccion" id="direccion"/></td>
                    </tr>   
                                    
                                                     
                     <tr>
                    	<td>Nombre de la empresa donde trabaja</td>
                        <td><input type="text"  required="required" name="nombre_empresa" id="nombre_empresa"/></td>
                    </tr>   
                       
                                     
                     <tr>
                    	<td>Telefono de su lugar de trabajo</td>
                        <td><input type="text"  required="required" name="telefono_empresa" id="telefono_empresa"/></td>
                    </tr>   
                                    
                                                     
                     <tr>
                    	<td>Numero de personas que viven en su casa</td>
                        <td><input type="number"  required="required" name="num_personas" id="num_personas"/></td>
                    </tr>   
                    
                                                     
                     <tr>
                    	<td>Edad de cada una de las personas</td>
                        <td><input type="text"  required="required" name="edades_personas" id="edades_personas"/></td>
                    </tr>   
                                    
                                                     
                     <tr>
                    	<td>¿Ha Padecido enfremedades algun familiar que vive en la casa?</td>
                        <td>
                            <select required="required" name="enferemedades" id="enfermedades">
                        		<option value="si">SI</option>
                                <option value="no">NO</option>
                                
                            </select></td>
                    </tr>   
                                    
                                     <tr>
                    	<td>Si su respuesta es si a la anterior, Describa la Enferemedad padecida:</td>
                        <td><input type="text"  required="required" name="enferemedades_padecidas" id="enferemedades_padecidas"/></td>
                    </tr>   
                                    
                                    <tr><TD><H3>SOBRE SU RESIDENCIA</H3></TD>
                                        <TD></TD></tr>
                             
                                     <tr>
                    	<td>Tipo de Casa:</td>
                        <td><input type="text"  required="required" name="tipo_casa" id="tipo_casa"/></td>
                    </tr>   
                             
                                     <tr>
                    	<td>¿Esta a su nombre la casa?</td>
                        <td><select required="required" name="propiedad" id="propiedad">
                        		<option value="si">SI</option>
                                <option value="no">NO</option>
                                
                            </select></td>
                    </tr>   
                        <tr>
                    	<td>¿Tiene Jardín?</td>
                        <td><select required="required" name="jardin" id="jardin">
                        		<option value="si">SI</option>
                                <option value="no">NO</option>
                                
                            </select></td>
                    </tr>     
                                    
                                    
                                        <tr>
                    	<td>¿Cual es la ubicacion del Jardin</td>
                        <td><select required="required" name="ubicacion_jardin" id="ubicacion_jardin">
                        		<option value="adelante">ADELANTE DE LA CASA</option>
                                <option value="atas">ATRAS DE LA CASA</option>
                                <option value="dentro">DENTRO DE LA CASA</option>
                                
                            </select></td>
                    </tr>   
                                    
                                    
                                      <tr><TD><H3>SOBRE SUS MASCOTAS ANTERIORES</H3></TD>
                                        <TD></TD></tr>
                                    
                                          <tr>
                    	<td>¿Ha tenido alguna vez mascotas?</td>
                          <td><select required="required" name="posecion_mascotas" id="posecion_mascotas">
                        		<option value="si">SI</option>
                                <option value="no">NO</option>
                                
                            </select></td>
                    </tr>   
                    
                               
                    
                                    
                       <tr>
                    	<td>¿Que tipo de mascotas ha tenido?</td>
                         <td><input type="text"  name="tipo_mascotas" id="tipo_mascotas"/></td>
                    </tr>           
                                    
                       <tr>
                    	<td>¿Cuantas mascotas tiene actualmente?</td>
                        <td><input type="number"  name="cantidad_mascotas_actual" id="cantidad_mascotas_actual"/></td>
                    </tr>    
                                    
                                                  
                       <tr>
                    	<td>¿Que tipo de mascotas tiene actualmente?</td>
                         <td><input type="text"  name="tipo_mascotas_actual" id="tipo_mascotas_actual"/></td>
                    </tr> 
                          
                       <tr>
                    	<td>¿Edad de las mascotas que tiene actualmente?</td>
                        <td><input type="number"  name="cantidad_mascotas_actual" id="cantidad_mascotas_actual"/></td>
                    </tr>    
                                    
                                        <tr>
                    	<td>¿Estan Vacunadas?</td>
                          <td><select required="required" name="vacunadas" id="vacunadas">
                        		<option value="si">SI</option>
                                <option value="no">NO</option>
                                
                            </select></td>
                    </tr>   
                                    
                                        <tr>
                    	<td>¿Estan Desparacitadas?</td>
                          <td><select required="required" name="desparacitadas" id="desparacitadas">
                        		<option value="si">SI</option>
                                <option value="no">NO</option>
                                
                            </select></td>
                    </tr>   
                                    
                                        <tr>
                    	<td>¿Estan Esterilizadas las mascotas?</td>
                          <td><select required="required" name="esterilizadas" id="esterilizadas">
                        		<option value="si">SI</option>
                                <option value="no">NO</option>
                                
                            </select></td>
                    </tr>   
                                    
                                    <tr>
                    	<td>si No tiene mascota ¿Que paso con  su ultima mascota</td>
                        <td><input type="text"  name="ultima_mascota" id="utlima_mascota"/></td>
                    </tr> 
                    
                    <tr>
                    	<td>¿Por que necesita una mascota?</td>
                        <td><input type="text"  name="necesidad_mascota" id="necesidad_mascota"/></td>
                    </tr> 
                    
                   <tr>
                    	<td>¿Donde tendra a la mascota de día?</td>
                        <td><input type="text"  name="ubicacion_mascota_dia" id="ubicacion_mascota_dia"/></td>
                    </tr> 
                    <tr>
                    	<td>¿Donde tendra a la mascota de noche?</td>
                        <td><input type="text"  name="ubicacion_mascota_noche" id="ubicacion_mascota_noche"/></td>
                    </tr> 
                    <tr>
                    	<td>¿Que tipo de actividad planea realizar con su nueva mascota?</td>
                        <td><input type="checkbox" name="actividad" value="visitas_veterinario"> visitas periodicas al veterinario<br/>
                        <input type="checkbox" name="actividad" value="desparacitacion"> desparacitación<br/>
                        <input type="checkbox" name="actividad" value="vacunas"> vacunas<br/></td>
                    </tr> 
                    
                    <tr>
                    	<td colspan="2">
                        	<div id="mensaje"></div>
                        </td>
                    </tr>
                </table>
            </div>
            
            <div class="modal-footer">
            	<input type="submit" value="Registrar" class="btn btn-success" id="reg"/>
                <input type="submit" value="Editar" class="btn btn-warning"  id="edi"/>
            </div>
            </form>
          </div>
        </div>
      </div>
    
    
 <footer>

<center>

         <br />
<img src="../recursos/logoumg300.png" widt="100px" height="100px"/>
<h5>UMG - FACULTAD DE INGENIERIA EN SISTEMAS</h5>
<h5>PROYECTO - ETICA 2015</h5>
<h5>DOCEAVO SEMESTRE</h5>
<h5>CONTACTO - SOPORTE TECNICO - TEL +502 30154416 -  CORREO: cjfn10101@gmail.com</h5>
</center>

</footer>
      

</body>
</html>
